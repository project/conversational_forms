CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration

INTRODUCTION
------------
Integration with <a href="https://space10.com/"><strong>Space10's</strong></a> <a href="https://github.com/space10-community/conversational-form/"><strong>Conversational Forms</strong></a> JS library.

This plug and play module converts a traditional webform into conversational interfaces. Every field can be individually configured for questions, errors and validations

<a href="https://medium.com/conversational-interfaces/introducing-the-conversational-form-c3166eb2ee2f#.vsoo5psud/"><strong>More reading</strong></a>

INSTALLATION
------------

The installation of this module is like other Drupal modules.

 1. Copy/upload the conversational_forms module to the modules directory of your Drupal installation.

 2. Enable the 'Conversational Forms' module in 'Extend'. (/admin/modules)

 3. Set up user permissions. (/admin/people/permissions#module-conversational_forms)

CONFIGURATION
-------------

 * Build a new webform (/admin/structure/webform) or duplicate an existing template (/admin/structure/webform/templates).

 * Convert webform into conversational form using third-party settings. (admin/structure/webform/manage/{form_name}/settings)

 * Option for individual fields can be found in  third-party settings under General Tab.

 * Publish your webform as a:

   - **Page** by linking to the published webform. (/webform/contact)
   - **Node** by creating a new node that references the webform. (/node/add/webform)
   - **Block** by placing a Webform block on your site. (/admin/structure/block)

Note: Conversational form only supports following elements
  * textfield
  * textarea
  * checkbox
  * email
  * radios
  * number
  * select
