<?php

namespace Drupal\conversational_forms\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Drupal\conversational_forms\ConversationalFormsBase;

/**
 * Submit a form without a page reload.
 */
class Example extends ConversationalFormsBase
{

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return $this->getBaseFormId() . '_example';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form['learn'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hi. Do you want to learn about Conversational forms?'),
    ];

    $form['copy'] = [
      '#type' => 'checkbox',
      '#title' => $this
        ->t('Send me a copy'),
      '#attributes' => [
        'cf-questions' => $this->t('Do you need a copy?'),
      ],
    ];

    $form['about_this_example'] = [
      '#type' => 'radios',
      '#title' => $this->t('About this example'),
      '#options' => ['yes' => 'Yes', 'no' => 'No'],
      '#attributes' => [
        'cf-questions' => $this->t('This is an example of conversational form built on drupal form API. You can browse the code of this page inside the module directory at "src/Form/Example.php". Follow the documentation to convert your custom forms to a form like this. Do you want to know how to theme it better?'),
      ],
    ];

    $form['how_to_theme'] = [
      '#type' => 'textfield',
      '#title' => $this->t('About this example'),
      '#attributes' => [
        'cf-questions' => $this->t('You can overwrite the CSS like any other page. You may need to use the "!important" to overwite certain parts.'),
      ],
    ];

    $form['more'] = [
      '#type' => 'radios',
      '#title' => $this->t('More about this example'),
      '#options' => ['ok' => 'Ok', 'cancel' => 'Cancel'],
      '#attributes' => [
        'cf-questions' => $this->t('To know more about the module, checkout the module page. If you find and isssue, please contribute to the issue queue at https://www.drupal.org/project/issues/conversational_forms?categories=All'),
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $this->messenger()->addStatus($this->t('Thank you for answering my questions'));
    $form_state->setRedirect('conversational_forms.admin_settings');
  }
}
