<?php

namespace Drupal\conversational_forms\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Defines a form that configures conversational_forms module settings.
 */
class ConversationalFormsConfiguration extends ConfigFormBase
{

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'conversational_forms.settings';

  /**
   * Config Keys.
   *
   * @var string[]
   */
  const IMAGES = ['robotImage', 'userImage'];

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'conversational_forms_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return [
      self::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $config = $this->config(self::SETTINGS);
    $file_upload_location = 'public://conversational_forms/';

    $form['images'] = [
      '#type' => 'markup',
    ];

    $robotIds  = $config->get('robotImage') ? [$config->get('robotImage')] : [];

    $form['images']['robotImage'] = [
      '#title' => $this->t('Robot Image'),
      '#type' => 'managed_file',
      '#description' => $this->t('If no file is uploaded, default will be used.'),
      '#upload_location' => $file_upload_location,
      '#default_value' => $robotIds,
    ];

    $anonymousIds  = $config->get('userImage') ? [$config->get('userImage')] : [];

    $form['images']['userImage'] = [
      '#title' => $this->t('User Image'),
      '#type' => 'managed_file',
      '#description' => $this->t('If no file is uploaded, default will be used.'),
      '#upload_location' => $file_upload_location,
      '#default_value' => $anonymousIds,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $config  = $this->config(self::SETTINGS);
    $configData = [];
    $newConfigData = [];
    foreach (self::IMAGES as $key) {
      $newImages[$key] = array($form_state->getValue([$key, 0]));
      $config->set($key, $form_state->getValue([$key, 0]));
    }

    conversational_forms_saveFiles($configData, $newConfigData, 'global');
    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   *
   */
  public static function getGlobalImages()
  {
    $config = \Drupal::config(self::SETTINGS);
    $settings = [];
    foreach (self::IMAGES as $key) {
      $fileId = (int)$config->get($key);
      if (!empty($fileId)) {
        $file = File::load($fileId);
        if ($file) {
          $settings[$key] = $file->createFileUrl(false);
        }
      }
    }
    return $settings;
  }
}
