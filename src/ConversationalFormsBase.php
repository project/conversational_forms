<?php

namespace Drupal\conversational_forms;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\BaseFormIdInterface;

/**
 * Submit a form without a page reload.
 */
abstract class ConversationalFormsBase extends FormBase implements BaseFormIdInterface
{

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId()
  {
    return 'conversational_forms';
  }
}
