/**
 * @file
 * Conversational forms behaviors.
 */

(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.conversational_forms = {
    attach: function (context) {
      $.each(drupalSettings.cForms, function (formId, formSettings) {
        const elements = once("cFormsProcessed", "#" + formId, context);
        console.log(elements);
        elements.forEach(function (item) {
          $(item).conversationalForm(formSettings);
        });
      });
    },
  };
})(jQuery, Drupal, drupalSettings);

