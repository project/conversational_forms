<?php

/**
 * @file
 * Integrates third party settings on the Conversational Forms module's behalf.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_webform_third_party_settings_form_alter().
 */
function conversational_forms_webform_third_party_settings_form_alter(&$form, FormStateInterface $form_state)
{
  /** @var \Drupal\webform\Entity\Webform $webform */
  $webform = $form_state->getFormObject()->getEntity();
  $status = $webform->getThirdPartySetting('cForms', 'status');
  $image = $webform->getThirdPartySetting('cForms', 'images');

  $file_upload_location = 'public://conversational_forms/';
  $form['third_party_settings']['cForms'] = [
    '#type' => 'details',
    '#title' => t('Conversational Forms Settings'),
    '#open' => TRUE,
  ];

  $form['third_party_settings']['cForms']['status'] = [
    '#type' => 'checkbox',
    '#title' => t('Convert this webform to conversational form.'),
    '#description' => t('If checked, a progress bar will displayed about the form.'),
    '#default_value' => $status,
  ];

  $form['third_party_settings']['cForms']['images'] = [
    '#type' => 'container',
    '#states' => [
      'visible' => [
        ':input[name="third_party_settings[cForms][status]"]' => [
          'checked' => TRUE
        ]
      ],
    ]
  ];

  $form['third_party_settings']['cForms']['images']['robotImage'] = [
    '#title' => t('Robot Image'),
    '#type' => 'managed_file',
    '#description' => t('If no file is uploaded, default will be used.'),
    '#upload_location' => $file_upload_location,
    '#default_value' => $image['robotImage'] ? $image['robotImage'] : [],
  ];

  $form['third_party_settings']['cForms']['images']['userImage'] = [
    '#title' => t('User Image'),
    '#type' => 'managed_file',
    '#description' => t('If no file is uploaded, default will be used.'),
    '#upload_location' => $file_upload_location,
    '#default_value' => $image['userImage'] ? $image['userImage'] : [],
  ];

  $form['#validate'][] = '_conversational_forms_form_validate';
}

function _conversational_forms_form_validate(&$form, FormStateInterface $form_state)
{

  if (!$form_state->getValue('op')) return;

  /** @var \Drupal\webform\Entity\Webform $webform */
  $webform = $form_state->getFormObject()->getEntity();
  $cFormsImages = $webform->getThirdPartySetting('cForms', 'images');
  $newImages = $form_state->getValue('third_party_settings')['cForms']['images'];
  conversational_forms_saveFiles($cFormsImages, $newImages, $webform->id());
}
